package com.nononsenseapps.feeder.ui.compose.empty

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier


//@Composable
//fun EmptyScreen(
//) {
//    // Keeping the Box behind so the scrollability doesn't override clickable
//    // Separate box because scrollable will ignore max size.
//    Box(
//        modifier = Modifier
//            .fillMaxSize()
//            .verticalScroll(rememberScrollState())
//    )
//    NothingToRead(
//        modifier = modifier,
//        onOpenOtherFeed = openNavDrawer,
//        onAddFeed = onAddFeed
//    )
//}
